#!/usr/bin/env python
#! -*- coding: utf8 -*-
import midi
class Oculta_midi:

    def __init__(self,nombre_archivo):
        self.nombre_archivo=nombre_archivo
        self.mensaje=""
        self.pattern=[]
        self.melodia_oculta=[]
        
    def codificar(self, mensaje):
        mensaje=mensaje.upper().replace("Ñ","#").replace("Á","A").replace("É","E").replace("Í","I").replace("Ó","O").replace("Ú","U")
        melodia_oculta=[]
        for letra in list(mensaje):
            melodia_oculta.append(midi.NoteOnEvent(tick=25, channel=0, data=[ord(letra), 0]))
       
        self.melodia_oculta=melodia_oculta
        
    def obtener_archivo(self):
        self.pattern=midi.read_midifile(self.nombre_archivo)
        
    def analizar(self):
        self.obtener_archivo()
        self.cantidad_tracks=len(self.pattern)
        self.cabecera=len(self.pattern[0])
        self.melodias=len(self.pattern[1:])
    
    def melodia_mas_larga(self):
        longitud=0
        melodia_mas_larga=""
        import pdb; pdb.set_trace()
        for melodia in self.pattern[1:]:
            if longitud <= len(melodia):
                melodia_mas_larga=melodia
        self.melodia_mas_larga=melodia_mas_larga
        return self.melodia_mas_larga
        
        
        
    def obtener_controles(self):
        controles_iniciales=[]
        controles_finales=[]
        for control in self.melodia_mas_larga:
            if isinstance(control, midi.ControlChangeEvent) or isinstance(control,midi.ProgramChangeEvent ):
                controles_iniciales.append(control)
            elif isinstance(control,midi.EndOfTrackEvent ):
                controles_finales.append(control)
        return {"iniciales":controles_iniciales, "finales":controles_finales}
        
        
    def armar_track(self):
        track=midi.Track()
        #import pdb; pdb.set_trace();
        partes=self.obtener_controles()
        track.extend(partes["iniciales"])
        track.extend(self.melodia_oculta)
        track.extend(partes["finales"])
        return track
     
    def generar_archivo(self):
        track=self.armar_track()
        self.pattern.append(track)
        nombre_archivo=self.nombre_archivo.split(".")[0]
        nuevo_nombre=nombre_archivo+"_oculto.mid"
        midi.write_midifile(nuevo_nombre, self.pattern)
      
