import midi
class Desocultar():
    
    def __init__(self,nombre_archivo):
        self.nombre_archivo=nombre_archivo
        self.mensaje_retorno=''
        self.pattern=[]
        self.track_oculta=[]
        
    def leer_archivo(self):
        self.pattern=midi.read_midifile(self.nombre_archivo)
        self.track_oculta=self.pattern[-1]
    
    def extraer_mensaje(self):
        mensaje=''
        for control in self.track_oculta:
            if isinstance(control, midi.NoteOnEvent):
                mensaje=mensaje+chr(control.pitch)
        #print mensaje
        return mensaje
