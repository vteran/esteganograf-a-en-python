#!/usr/bin/env python
from Ocultamiento import Oculta_midi
from Desocultar import Desocultar
import sys
from os import path  

nombre_archivo=sys.argv[1]
comando=sys.argv[2]

if comando=="-o":
    if path.isfile(nombre_archivo):
        partes=nombre_archivo.split(".")
        ext=partes[-1]
        if ext != "mid":
            print "Archivo invalido"
        else:
            ocultar=Oculta_midi(nombre_archivo)
            ocultar.analizar()
            log_max=len(ocultar.melodia_mas_larga())
            msg="Ingrese un mensaje no mayor a %d" % (log_max)
            mensaje_a_ocultar=raw_input(msg)
            ocultar.codificar(mensaje_a_ocultar)
            ocultar.generar_archivo()
    else:
        print "Debe indicar un archivo existente" 

elif comando=="-d":
    
    if path.isfile(nombre_archivo):
        decodificar=Desocultar(nombre_archivo)
        decodificar.leer_archivo()
        print "El mensaje oculto es: %s" % decodificar.extraer_mensaje()
    else:
        print "Debe indicar un archivo existente" 

else:
    print "Comando no valido"
    



#####################




